#!/bin/bash
#
# @@script: init.sh
# @@description: squeezer startup
# @@author: Loouis Low
# @@copyright: GNU GPL
#

function start_squeezer() {
    python squeezer.py
}

### init
start_squeezer
