# squeezer

> Multi-processing image compressor

# Concept

- Set up passive image optimization for cronjob at server

# Usage

Set up system wide.

```bash
$ sudo ln -s squeezer.py /usr/local/bin
```
```bash
$ sudo mv /usr/local/bin/squeezer.py /usr/local/bin/squeezer
```

Run,

```bash
$ squeezer -v -d /path/to/image/
```

Or just,

```bash
$ python squeezer.py -v -d /path/to/image/
```

## Options

**-v, --version**	`show version`

**-h, --help**	`show this help message`

**-q, --quiet**	`verbose mode (default)`

**-f FILENAME, --file=FILENAME**	`compress image`

**-d DIRECTORY, --directory=DIRECTORY**	`compress image in directory`
